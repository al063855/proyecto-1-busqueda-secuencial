package arreglo_bidimensional;

import java.util.Scanner;
/**
 *
 * @author hitzu
 */
public class Arreglo_bidimensional {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
   
        // TODO code application logic here
        Arreglo_bidimensional obj = new Arreglo_bidimensional();
        Scanner sc = new Scanner(System.in);
        int arru [][] = new int [4][4];
        arru [0][0] = 10;
        arru [0][1] = 7;
        arru [1][0] = 8;
        arru [1][1] = 4;
        arru [2][0] = 3;
        arru [2][1] = 9;
        arru [2][2] = 2;
        arru [3][0] = 1;
        arru [3][1] = 6;
        arru [3][2] = 11;
        arru [3][3] = 20;
        
        int dato = 0;
        System.out.println("Ingrese el número a buscar");
        dato = sc.nextInt();
       
        Arreglo_bidimensional.busquedaSecuencial(arru, dato);
        
    }
    public static void busquedaSecuencial(int [][]arreglo,int dato){
    
    for(int i = 0; i < arreglo.length; i++){//recorremos todo el arreglo
        for (int j = 0; j < arreglo.length; j++) {
            if(arreglo[i][j] == dato){
            System.out.println("El valor se encuentra en la posición ["+i+"]["+j+"]");
		break;
            }
            if(arreglo[i][j] != dato){
            System.out.println("El valor no se encuentra");
            }
        }
 }
}    
}
